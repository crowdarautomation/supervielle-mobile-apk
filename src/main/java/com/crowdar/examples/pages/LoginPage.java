package com.crowdar.examples.pages;

import org.openqa.selenium.WebElement;

import com.crowdar.bdd.cukes.SharedDriver;
import com.crowdar.mobile.core.PageBase;

import io.appium.java_client.MobileBy;

public class LoginPage extends PageBase{

	private static final String USERNAME_INPUT_XPATH = "//*[@resource-id='username']";
	private static final String PASSWORD_INPUT_XPATH = "//*[@resource-id='password']";
	private static final String LOGIN_BUTTON_XPATH  =  "//*[@resource-id='btn_ingresar']";
	private static final String BACK_XPATH  =  "//*[@resource-id='backButton']";
	
    public LoginPage(SharedDriver driver){
        super(driver);
        this.url = "";
    }

   public void login(String username, String password){
	   
	   WebElement usernameInput = getMobileElement(MobileBy.xpath(USERNAME_INPUT_XPATH));
	   usernameInput.click();
	   
	   // workaround
	   getMobileElement(MobileBy.xpath(BACK_XPATH)).click();
	   usernameInput = getMobileElement(MobileBy.xpath(USERNAME_INPUT_XPATH));
	   usernameInput.click();
	   //
	   
	   usernameInput.sendKeys(username);
	   
	   
	   WebElement passwordInput = getMobileElement(MobileBy.xpath(PASSWORD_INPUT_XPATH));
	   passwordInput.click();
	   passwordInput.sendKeys(password);
	   
       getMobileElement(MobileBy.xpath(LOGIN_BUTTON_XPATH)).click();
   }
   
   
}