package com.crowdar.examples.pages;

import com.crowdar.bdd.cukes.SharedDriver;
import com.crowdar.mobile.core.PageBase;

import io.appium.java_client.MobileBy;

public class Inicio extends PageBase{

	private static final String LOGIN_PEOPLE_BUTTON_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View[1]/android.view.View[1]";
	

    public Inicio(SharedDriver driver){
        super(driver);
        this.url = "";
    }

   public void clickLoginIndividuos(){
	   getMobileElement(MobileBy.xpath(LOGIN_PEOPLE_BUTTON_XPATH)).click();
   }

}