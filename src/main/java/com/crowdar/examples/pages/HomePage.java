package com.crowdar.examples.pages;

import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.crowdar.bdd.cukes.SharedDriver;
import com.crowdar.mobile.core.PageBase;

public class HomePage extends PageBase{

	private static final String USER_HEADER_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[1]";
//	private static final String SPINNER_XPATH = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View/android.widget.Image";
	

    public HomePage(SharedDriver driver){
        super(driver);
        this.url = "";
    }

   public String getHeaderUser(){
//	   getWait().until((Function<? super WebDriver, ? extends Object>) ExpectedConditions.invisibilityOfElementLocated(By.xpath(SPINNER_XPATH)));
	   RemoteWebElement user = (RemoteWebElement)getWait().until((Function<? super WebDriver, ? extends Object>) ExpectedConditions.presenceOfElementLocated(By.xpath(USER_HEADER_XPATH)));
	   return user.getText();
   }



}
