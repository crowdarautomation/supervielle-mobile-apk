package com.crowdar.examples.steps;

import org.testng.Assert;

import com.crowdar.bdd.cukes.SharedDriver;
import com.crowdar.core.PageSteps;
import com.crowdar.examples.pages.HomePage;
import com.crowdar.examples.pages.Inicio;
import com.crowdar.examples.pages.LoginPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class InicioSteps extends PageSteps {

    private Inicio inicio;
    private LoginPage loginPage;
    private HomePage homePage;

    public InicioSteps(SharedDriver driver){
        super(driver);
        inicio = new Inicio(driver);
        loginPage = new LoginPage(driver);
        homePage = new HomePage(driver);
    }

    @Given("el cliente individuo esta en la apk supervielle")
    public void goToApk() throws InterruptedException{
    }

    
    @When("el cliente {string} se logea con usuario: {string} y password: {string}")
    public void login(String userType, String username, String password) throws InterruptedException{
    	Thread.sleep(10000l);
    	inicio.clickLoginIndividuos();
    	loginPage.login(username, password);
    }

    
    @Then("el cliente individuo {string} accedio a la home")
    public void homepageVerification(String user) throws InterruptedException{
    	Thread.sleep(10000l);
    	Assert.assertEquals(homePage.getHeaderUser(), user);
    }
}
