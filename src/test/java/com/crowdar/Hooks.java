package com.crowdar;

import org.testng.annotations.AfterTest;

import com.crowdar.driver.DriverManager;
import cucumber.api.java.After;



public class Hooks {

    @AfterTest
    public void afterScenario(){
        DriverManager.dismissCurrentDriver();
    }

}
