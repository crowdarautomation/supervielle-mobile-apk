#Supervielle mobile apk POC
##Requirements:
* -git client   https://www.atlassian.com/git/tutorials/install-git
* -docker-compose 1.24.0 ( https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04 --> only step 1 from tutorial)
* -docker (https://phoenixnap.com/kb/how-to-install-docker-on-ubuntu-18-04)

##Steps:
1. Clone source project     
	```git clone https://bitbucket.org/crowdarautomation/supervielle-mobile-apk.git```   

2. From this location(clone directory) execute
    ```sudo docker-compose up```  
		this command starts the solution
			-  jenkins container (http://localhost:8080)
			-  selenium grid container (http://localhost:4444)
			-  docker android container (contains appium server, android emulator, novnc(http://localhost:6081))
			
3. Now you be able to run tests.
	- Open a web browser and go to jenkins (http://localhost:8080). 
	- Login as admin, user:admin password:secret 
	- Execute the job and go to console output to view de execution log.
	
4. To view the interaction between tests and emulated device by accesing at http://localhost:6081/

4. When the execution has finished, the output report can be accessed at **[PROJECT_ROOT]/target/cucumber-report**
	
	
		
	
	
	  
	
	
	    















